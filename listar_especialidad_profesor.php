<?php
include("includes/header.php");
require_once("conection/conexion.php");
$x= $_GET['id'];
try{
  $sql=$conexion->prepare("SELECT * FROM Profesor p, Usuarios u WHERE p.id_us='$x' and u.id='$x'");
                      $sql->execute();
                      if($fila = $sql->fetch()){
                        ?>
<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo strtoupper($fila['id_prof']); ?> Especialidades de Prof. <?php echo strtoupper($fila['username']);?><small></small></h2>
                    <a type="button"  class="btn btn-info" data-toggle="modal" data-target="#form1" class="btn btn-success"></i>Adicionar</a>
                  <br>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>hp
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <!-- modal nueva especialidad -->
                  <div class="modal fade" id="form1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-dialog-centered">
                          <div class="modal-content">
                              <div class="modal-header border-bottom-0">
                                  <h5 class="modal-title" id="exampleModalLabel">Intro informacion</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                  </button>
                              </div>
                              <div class="modal-body">
                                  <form class="form-horizontal form-label-left" action="alta_especialidad.php" method="POST" novalidate >
                                      <input  value="<?php echo $fila['id_us']?>" name="id_us" hidden>
                                      <input  value="<?php echo $fila['id_prof']?>" name="id_profe" hidden>
                                      <div class="modal-body">
                                          <div class="item form-group">
                                              <div class="col-md-6 col-sm-6 col-xs-12">
                                                  <input id="especialidad" class="form  -control col-md-7 col-xs-12" name="especialidad" placeholder="Especialidad" required="required" type="text">
                                              </div>
                                          </div class="ln_solid"></div>
                                      </div>
                                      <div class="modal-footer border-top-0 d-flex justify-content-center">
                                          <div class="form-group">
                                                  <a href="listar_especialidad_profesor.php" class="btn btn-danger" data-dismiss="modal">Cancel</a>
                                                  <button type="submit" class="btn btn-success">Submit</button>
                                          </div>
                                      </div>
                                  </form>
                              </div>
                      </div>
                  </div>
                  </div>
                  <!-- fin modal nueva especialidad -->

                  <div class="x_content">
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Especialidad</th>
                          <th>Operaciones</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                                $sql1=$conexion->prepare("SELECT e.*, p.* FROM Especialidad e, Profesor p WHERE p.id_us='{$_GET['id']}' and e.id_profe=p.id_prof");
                                $sql1->execute();
                                while($fila1 = $sql1->fetch()){?>
                                    <tr>
                                        <td>
                                          <span><?php echo $fila1['especialidad'];?></span>
                                        </td>
                                        <td>
                                          <a href="#edit_esp_<?php echo $fila1['id_especialidad']; ?>" class="btn btn-info btn-xs" data-toggle="modal"><span class="fa fa-pencil"></span>Edit</a>
                                          <a href="#eliminar_especialidad.php?id_especialidad=<?php echo urlencode($fila1['id_especialidad']);?>&id_us=<?php echo urlencode($fila['id_us']);?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Eliminar </a>
                                        </td>
                                        <?php include("modal_especialidad.php"); ?>
                                    </tr>
                                <?php
                                }
                            ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
<?php
}
}    catch(PDOException $e){
  print "Error: ".$e->getMessage()."<br/>";
  die();
}
include("includes/footer.php");?>
