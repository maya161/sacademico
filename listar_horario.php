<?php
  include("includes/header.php");
  require_once("conection/conexion.php");
  require_once("function/consulta_horario.php");
?>
<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>NIVEL PRIMARIA<small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Curso</th>
                          <th>Paralelo</th>
                          <th>Operaciones</th>
                        </tr>
                      </thead>
                      <tbody>

                        <?php

                            try{
                                $sql=$conexion->prepare("SELECT * FROM Curso WHERE nivel='primaria'");
                                $sql->execute();
                                while($fila = $sql->fetch()){?>
                                    <tr>
                                        <td><?php echo $fila['curso'];?></td>
                                        <td><?php echo $fila['paralelo'];?></td>
                                        <td>
                                        <a href="mostrar_horario.php?id_curso=<?php echo urlencode($fila['id_curso']); ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Mostrar Horario</a>
                                        </td>
                                      </tr>
                                <?php
                          }
                            ?>
                      </tbody>
                    </table>
                  </div>
                  <?php
}    catch(PDOException $e){
      print "Error: ".$e->getMessage()."<br/>";
      die();
              }  ?>

                  <h2>NIVEL SECUNDARIA<small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  
                  <div class="x_content"> 
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Curso</th>
                          <th>Paralelo</th>
                          <th>Operaciones</th>
                        </tr>
                      </thead>
                      <tbody>

                        <?php

                            try{
                                $sql=$conexion->prepare("SELECT * FROM Curso WHERE nivel='secundaria'");
                                $sql->execute();

                                while($fila = $sql->fetch()){?>
                                    <tr>
                                    <tr>
                                        <td><?php echo $fila['curso'];?></td>
                                        <td><?php echo $fila['paralelo'];?></td>
                                        <td>
                                          <a href="mostrar_horario.php?id_curso=<?php echo urlencode($fila['id_curso']); ?> " class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Mostrar Horario</a>
                                        </td>
                                      </tr>
                                      </tr>
                                <?php
                          }
                            ?>
                      </tbody>
                    </table>
                  </div>
                </div> 
              </div>
<?php
}    catch(PDOException $e){
      print "Error: ".$e->getMessage()."<br/>";
      die();
              }
 include("includes/footer.php");?>
