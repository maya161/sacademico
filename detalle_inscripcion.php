<?php 
include("includes/header.php");
require_once("conection/conexion.php");
try{
    $sql=$conexion->prepare("SELECT m.*, a.* FROM Matricula m, Alumno a WHERE m.id_al={$_GET['id_alumno']} and a.id_alumno={$_GET['id_alumno']}");
    $sql->execute();
    if($fila = $sql->fetch()){ ?>

    
                  <div class="x_content">
                    <form class="form-horizontal form-label-left" novalidate >
                      <span class="section"><?php echo $fila[nombre]." ".$fila[apellido]?></span>

                    <?php
                        if($fila['tag']==1){?>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Estado:
                                </label>
                                <div class="col-md-6 col-sm-6 col-xl-12 mb-2">
                                    <span class="text-info " ><?php echo "Inscrito";?></span>
                                </div>
                            </div>


                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Turno:
                        </label>
                        <div class="col-md-6 col-sm-6 col-xl-12 mb-2">
                            <span class="text-info " ><?php echo $fila['turno'];?></span>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nivel:
                        </label>
                        <div class="col-md-6 col-sm-6 col-xl-12 mb-2">
                            <span class="text-info " ><?php echo $fila['nivel'];?></span>
                        </div>
                    </div>
            
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Grado:
                        </label>
                        <div class="col-md-6 col-sm-6 col-xl-12 mb-2">
                            <span class="text-info " ><?php echo $fila['grado'];?></span>
                        </div>
                    </div>

                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Paralelo:
                        </label>
                        <div class="col-md-6 col-sm-6 col-xl-12 mb-2">
                            <span class="text-info " ><?php echo $fila['paralelo'];?></span>
                        </div>
                    </div>
                    <?php
                        }else{
                            if($fila['tag']==0){ ?>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Estado:
                                </label>
                                <div class="col-md-6 col-sm-6 col-xl-12 mb-2">
                                    <span class="text-info " ><?php echo "No inscrito";?></span>
                                </div>
                            </div>

                    <?php
                        }
                    }
                    ?>
                      <div class="ln_solid"></div>
                    </form>
                  </div>
                </div>

<?php
}
}
catch(PDOException $e){
    print "Error: ".$e->getMessage()."<br/>";
}
include("includes/footer.php");
?>