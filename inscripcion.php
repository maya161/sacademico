<?php
  include("includes/header.php");
    require_once("conection/conexion.php");
?>
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Lista de Estudiantes<small></small></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a> </li>
        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a></li>
            <li><a href="#">Settings 2</a></li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a></li>
      </ul>
      <div class="clearfix"></div>
    </div>

  <div class="x_content">
    <table id="datatable" class="table table-striped table-bordered">
      <thead>
        <tr>
          <th>Nombre</th>
          <th>Apellido</th>
          <th>CI</th>
          <th>Operaciones</th>
        </tr>
      </thead>
      <tbody>
        <?php
          try{
            $sql=$conexion->prepare("SELECT * FROM Alumno");
            $sql->execute();

            while($fila = $sql->fetch()){
              ?>
              <tr>
                  <td><?php echo $fila['nombre'];?></td>
                  <td><?php echo $fila['apellido'];?></td>
                  <td><?php echo $fila['ci'];?></td>
                  <td>
                    <?php
                      if( $fila['tag']=='0'){?>
                        <a href="form_inscripcion_alumno1.php?id=<?php echo urlencode($fila['id_alumno']);?>" class="btn btn-success btn-xs"><i class="fa fa-pencil"></i> Inscribir </a>
                    <?php
                      }else{
                        if($fila['tag']=='1'){ ?>
                          <a href="#" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Inscribir </a>
                    <?php
                        }
                      }
                    ?>
                      <a href="eliminar_inscripcion.php?id_alumno=<?php echo urlencode($fila['id_alumno']);?>" class="btn btn-danger btn-xs"><i class="fa fa-pencil"></i> Eliminar inscripcion </a>
                      <a href="detalle_inscripcion?id_alumno=<?php echo urlencode($fila['id_alumno']);?>" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> Detalles </a>
                      <button type="button" class="btn btn-primary btn-xs edit" value="<?php echo $fila['id_alumno']; ?>"><i class="fa fa-eye"></i> Detalles</button>

<!--                       <a type="button"  class="btn btn-primary btn-xs" data-toggle="modal" data-target="#form" ><i class="fa fa-eye"></i>Detalles</a>
 -->
                        <div class="modal fade" id="form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                              <div class="modal-header border-bottom-0">
<!--                             <h5 class="modal-title" id="exampleModalLabel">Info Inscripcion</h5>
 -->                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>

                              <form class="form-horizontal form-label-left" novalidate >
                              <?php try{
                                $sql1=$conexion->prepare("SELECT m.*, a.* FROM Matricula m, Alumno a WHERE m.id_al={$fila['id_alumno']} and a.id_alumno={$fila['id_alumno']}");
                                $sql1->execute();
                                if($fila1 = $sql1->fetch()){ ?>
                                  <span class="section"><?php echo $fila1['nombre']." ".$fila1['apellido']?></span>
                                <?php
                                    if($fila1['tag']==1){?>
                                      <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Estado:</label>
                                            <div class="col-md-6 col-sm-6 col-xl-12 mb-2">
                                                <span class="text-info " ><?php echo "Inscrito";?></span>
                                            </div>
                                      </div>
                                      <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Turno:
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xl-12 mb-2">
                                          <span class="text-info " ><?php echo $fila1['turno'];?></span>
                                        </div>
                                      </div>
                                      <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nivel:
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xl-12 mb-2">
                                          <span class="text-info " ><?php echo $fila1['nivel'];?></span>
                                        </div>
                                      </div>
                                      <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Grado:
                                        </label>
                                          <div class="col-md-6 col-sm-6 col-xl-12 mb-2">
                                            <span class="text-info " ><?php echo $fila1['grado'];?></span>
                                          </div>
                                      </div>

                                      <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Paralelo:
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xl-12 mb-2">
                                          <span class="text-info " ><?php echo $fila1['paralelo'];?></span>
                                        </div>
                                      </div>
                                <?php
                                    }else{
                                        if($fila1['tag']==0){ ?>
                                          <div class="item form-group">
                                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Estado:
                                              </label>
                                              <div class="col-md-6 col-sm-6 col-xl-12 mb-2">
                                                  <span class="text-info " ><?php echo "No inscrito";?></span>
                                              </div>
                                          </div>
                                <?php
                                        }
                                    }
                                ?>
                                <div class="ln_solid"></div>
                                </form>
                           <?php
                                }
                              }
                              catch(PDOException $e){
                                print "Error: ".$e->getMessage()."<br/>";
                              }
                            ?>

                                </div>
                              </div>
                            </div>
              <br>

              </td>
            </tr>
              <?php
                }
              ?>
        </tbody>
      </table>
    </div>

  </div>
</div>
<?php
}    catch(PDOException $e){
      print "Error: ".$e->getMessage()."<br/>";
      die();
              }
 include("includes/footer.php");?>