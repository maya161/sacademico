<?php 
include("includes/header.php");
require_once("conection/conexion.php");
?>    
<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Lista de Inscritos<small></small></h2>
                    <a href="form_adicionar_usuario2.php" class="btn btn-success "> Adicionar</a>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  
                  <div class="x_content">

                  <div class="container text-center">
                    <div class="row ">
                        <div class="col border-bottom border-dark">
                            <h3 class="text-center text-dark">TURNO MAÑANA</h3>
                            <h4>Nivel: Primario<small></small></h4>                            
                        </div>
                    </div>
                    <h4>Grado: 1<small></small></h4>                            
                
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                            <th>CI</th>
                            <th>Apellido</th>
                            <th>Nombre</th>
                        </tr>
                      </thead>
                      <tbody>

                        <?php
                            try{
                                $sql=$conexion->prepare("SELECT a.*, m.* FROM Alumno a, Matricula m WHERE turno='mañana' and nivel='primario'");
                                $sql->execute();

                                while($fila = $sql->fetch()){?>
                                    <tr>
                                        <td><?php echo $fila['ci'];?></td>
                                        <td><?php echo $fila['Apellido'];?></td>
                                        <td><?php echo $fila['Nombre'];?></td>
                                      </tr>
                                <?php
                          }
                            ?>
                                    </tbody>
                                  </table>
                                </div>

                              </div>
                            </div>
<?php
}    catch(PDOException $e){
      print "Error: ".$e->getMessage()."<br/>";
      die();
              }
 include("includes/footer.php");?>