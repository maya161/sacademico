<?php 
include("includes/header.php");
require_once("conection/conexion.php");

try{
$sql=$conexion->prepare("SELECT u.*, a.* FROM Usuarios u, Alumno a  WHERE u.id={$_GET['id']} and a.id_us={$_GET['id']}");
                    $sql->execute();  
                    if($fila = $sql->fetch()){ ?>

                  <div class="x_content">

                    <form class="form-horizontal form-label-left" novalidate action="update_alumno.php" method="POST">
                      <span class="section">Editar informacion</span>
                      <input name="id" type="number" value="<?php echo $fila['id'];?>" hidden>
                      <input name="id" type="number" value="<?php echo $fila['id_us'];?>" hidden>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Username <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="username" placeholder="both name(s) e.g Jon Doe" required="required" type="text" value="<?php echo $fila['username'];?>">
                        </div>
                      </div>
                          
                      <div class="item form-group">
                            <label for="password" class="control-label col-md-3">Password</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input id="password" type="password" name="password" data-validate-length="6,8" class="form-control col-md-7 col-xs-12" required="required" value="<?php echo $fila['password'];?>">
                            </div>
                          </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Role <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="role" name="role" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $fila['role'];?>">
                        </div>
                      </div>


                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="email" name="email" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $fila['correo_electronico'];?>">
                        </div>
                      </div>
                    
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Nombre <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="nombre" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $fila['nombre'];?>">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Apellido <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="apellido" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $fila['apellido'];?>">
                        </div>
                      </div>
                  
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Fecha de nacimiento<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="date" id="fecha_nacimiento" name="fecha_nacimiento" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $fila['fecha_nacimiento'];?>">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Direccion <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="direccion" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $fila['direccion'];?>">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Telefono <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="number" name="telefono" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $fila['telefono'];?>">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">CI <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="number" name="ci" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $fila['ci'];?>">
                        </div>
                      </div>

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <button type="submit" class="btn btn-primary">Cancel</button>
                          <button id="send" type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>

                <?php }
                }
                catch(PDOException $e){
                    print "Error: ".$e->getMessage()."<br/>";
                    
                }
                ?>


    
<?php 
include("includes/footer.php");
?>