<?php 
include("includes/header.php");
require_once("conection/conexion.php");

try{
$sql=$conexion->prepare("SELECT u.*, a.* FROM Usuarios u, Administrador a  WHERE u.id={$_GET['id']} and a.id_us={$_GET['id']}");
                    $sql->execute();  
                    if($fila = $sql->fetch()){ ?>

                  <div class="x_content">

                    <form class="form-horizontal form-label-left" novalidate action="update_administrador.php" method="POST">
                      <span class="section">Editar informacion</span>
                      <input name="id" type="number" value="<?php echo $fila['id'];?>" hidden>
                      <input name="id" type="number" value="<?php echo $fila['id_us'];?>" hidden>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Username <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="name" class="form-control col-md-7 col-xs-12" name="username" placeholder="both name(s) e.g Jon Doe" required="required" type="text" value="<?php echo $fila['username'];?>">
                        </div>
                      </div>
                          
                      <div class="item form-group">
                            <label for="password" class="control-label col-md-3">Password</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input id="password" type="password" name="password" data-validate-length="6,8" class="form-control col-md-7 col-xs-12" required="required" value="<?php echo $fila['password'];?>">
                            </div>
                          </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Role <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="role" name="role" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $fila['role'];?>">
                        </div>
                      </div>


                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="email" name="email" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $fila['correo_electronico'];?>">
                        </div>
                      </div>
                    
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">No. Item <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="no_item" name="no_item" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $fila['nro_item'];?>">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">CI <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="number" id="ci" name="ci" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $fila['ci'];?>">
                        </div>
                      </div>
                  
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Turno<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="turno" name="turno" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $fila['turno'];?>">
                        </div>
                      </div>

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <button type="submit" class="btn btn-primary">Cancel</button>
                          <button id="send" type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>

                <?php }
                }
                catch(PDOException $e){
                    print "Error: ".$e->getMessage()."<br/>";
                    
                }
                ?>


    
<?php 
include("includes/footer.php");
?>