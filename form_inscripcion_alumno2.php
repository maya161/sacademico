<?php
include("includes/header.php");
require_once("conection/conexion.php");
$niv=$_POST['nivel'];
?>
<div class="x_content">
        <form class="form-horizontal form-label-left" action="form_inscripcion_alumno3.php" method="POST" novalidate >
            <input type="text" name="id_al" value="<?php echo $_POST['id_al'];?>" hidden>
            <input type="text" name="fecha_matricula" value="<?php echo $_POST['fecha_matricula'];?>" hidden>
            <input type="text" name="turno" value="<?php echo $_POST['turno'];?>" hidden>
            <input type="text" name="nivel" value="<?php echo $_POST['nivel'];?>" hidden>

            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Grado<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <select name="grado">
                    <option value="">Elija una opcion</option>
                        <?php
                            try{
                                $sql=$conexion->prepare("SELECT DISTINCT curso FROM Curso WHERE nivel='$niv' ");
                                $sql->execute();

                                while($fila = $sql->fetch()){?>
                                    <option value="<?php echo $fila['curso']?>"><?php echo $fila['curso']?></option>
                          <?php
                                }
                            }    catch(PDOException $e){
                            print "Error: ".$e->getMessage()."<br/>";
                            die();
                        }?>
                    </select>

<!--                           <input type="text" name="grado" required="required" class="form-control col-md-7 col-xs-12">
 -->            </div>
            </div>

            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-md-offset-3">
                <button type="submit" class="btn btn-lg btn-success">Next</button>
              </div>
            </div>

        </form>
    </div>
</div>

<?php
include("includes/footer.php");
?>
