<?php 
include("includes/header.php");
require_once("conection/conexion.php");
try{
    $sql=$conexion->prepare("SELECT u.*, a.* FROM Usuarios u, Alumno a WHERE id={$_GET['id']} and id_us={$_GET['id']}");
    $sql->execute();  
    if($fila = $sql->fetch()){ ?>
                  <div class="x_content">
                    <form class="form-horizontal form-label-left" novalidate >
                      <span class="section">Personal Info</span>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Username:
                        </label>
                        <div class="col-md-6 col-sm-6 col-xl-12 mb-2">
                            <span class="text-info " ><?php echo $fila['username'];?></span>
                        </div>
                    </div>

                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Role:
                        </label>
                        <div class="col-md-6 col-sm-6 col-xl-12 mb-2">
                            <span class="text-info " ><?php echo $fila['role'];?></span>
                        </div>
                    </div>

                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Email:
                        </label>
                        <div class="col-md-6 col-sm-6 col-xl-12 mb-2">
                            <span class="text-info " ><?php echo $fila['correo_electronico'];?></span>
                        </div>
                    </div>

                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nombre:
                        </label>
                        <div class="col-md-6 col-sm-6 col-xl-12 mb-2">
                            <span class="text-info " ><?php echo $fila['nombre'];?></span>
                        </div>
                    </div> 

                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Apellido:
                        </label>
                        <div class="col-md-6 col-sm-6 col-xl-12 mb-2">
                            <span class="text-info " ><?php echo $fila['apellido'];?></span>
                        </div>
                    </div>               
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Fecha de nacimiento:
                        </label>
                        <div class="col-md-6 col-sm-6 col-xl-12 mb-2">
                            <span class="text-info " ><?php echo $fila['fecha_nacimiento'];?></span>
                        </div>
                    </div>
            
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Direccion:
                        </label>
                        <div class="col-md-6 col-sm-6 col-xl-12 mb-2">
                            <span class="text-info " ><?php echo $fila['direccion'];?></span>
                        </div>
                    </div>

                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Telefono:
                        </label>
                        <div class="col-md-6 col-sm-6 col-xl-12 mb-2">
                            <span class="text-info " ><?php echo $fila['telefono'];?></span>
                        </div>
                    </div>

                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">CI:
                        </label>
                        <div class="col-md-6 col-sm-6 col-xl-12 mb-2">
                            <span class="text-info " ><?php echo $fila['ci'];?></span>
                        </div>
                    </div>

                      <div class="ln_solid"></div>
                      
                    </form>
                  </div>
                </div>

<?php
}
}
catch(PDOException $e){
    print "Error: ".$e->getMessage()."<br/>";
    
}
include("includes/footer.php");
?>