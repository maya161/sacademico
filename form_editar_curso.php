<?php 
include("includes/header.php");
require_once("conection/conexion.php");

try{
    $y= $_GET['id_curso'];
    $sql=$conexion->prepare("SELECT * FROM Curso  WHERE id_curso='$y'");
                    $sql->execute();
                    if($fila = $sql->fetch()){ ?>

                  <div class="x_content">

                    <form class="form-horizontal form-label-left" novalidate action="update_curso_primaria.php" method="POST">
                      <span class="section">Editar informacion</span>
                      <input name="id_curso" type="number" value="<?php echo $fila['id_curso'];?>" hidden>
                    
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="paralelo">Curso <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="curso" class="form-control col-md-7 col-xs-12" name="curso" required="required" type="text" value="<?php echo $fila['curso'];?>">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="paralelo">Paralelo <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="paralelo" class="form-control col-md-7 col-xs-12" name="paralelo" required="required" type="text" value="<?php echo $fila['paralelo'];?>">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cupo">Cupo <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="cupo" class="form-control col-md-7 col-xs-12"  name="cupo" required="required" type="text" value="<?php echo $fila['cupo'];?>">
                        </div>
                      </div>

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <button type="submit" class="btn btn-primary">Cancel</button>
                          <button id="send" type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>

                <?php }
                }
                catch(PDOException $e){
                    print "Error: ".$e->getMessage()."<br/>";
                }
                ?>
<?php
include("includes/footer.php");
?>