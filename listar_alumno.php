<?php 
include("includes/header.php");
require_once("conection/conexion.php");
?>
<div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <?php
                if(isset($_GET['var'])){
                  if($_GET['var']==1){
                    ?>
                  <div class="alert alert-success alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
                  <strong>Congratulations!</strong>You successfully adition
                  </div>
                  <?php }else{
                      if($_GET['var']==2){

                    ?>
                  <div class="alert alert-danger alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
                  <strong>Danger!</strong>
                  </div>
                  <?php }
                  }?>
     <div class="x_title">
                    <h2>Lista de Estudiantes<small></small></h2>
                    <a href="form_adicionar_usuario2.php" class="btn btn-success "> Adicionar</a>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Username</th>
                          <th>password</th>
                          <th>Role</th>
                          <th>Email</th>
                          <th>Estado</th>
                          <th>Operaciones</th>
                        </tr>
                      </thead>
                      <tbody>

                        <?php
                            try{
                                $sql=$conexion->prepare("SELECT * FROM Usuarios WHERE role='estudiante'");
                                $sql->execute();

                                while($fila = $sql->fetch()){?>
                                    <tr>
                                        <td><?php echo $fila['username'];?></td>
                                        <td><?php echo $fila['password'];?></td>
                                        <td><?php echo $fila['role'];?></td>
                                        <td><?php echo $fila['correo_electronico'];?></td>
                                        <td>desactivo</td>
                                        <td>
                                          <a href="detalle_alumno.php?id=<?php echo urlencode($fila['id']); ?>" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> Detalle </a>
                                          <a href="form_editar_alumno.php?id=<?php echo urlencode($fila['id']); ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Editar </a>
                                          <a href="eliminar_alumno.php?id=<?php echo urlencode($fila['id']); ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Eliminar </a>
                                        </td>
                                      </tr>
                                <?php
                                } ?>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                    <?php }
      catch(PDOException $e){
        print "Error: ".$e->getMessage()."<br/>";
        die();
                }
                
                }else{ ?>
     <div class="x_title">
                    <h2>Lista de Estudiantes<small></small></h2>
                    <a href="form_adicionar_usuario2.php" class="btn btn-success "> Adicionar</a>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  
                  <div class="x_content">
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Username</th>
                          <th>password</th>
                          <th>Role</th>
                          <th>Email</th>
                          <th>Estado</th>
                          <th>Operaciones</th>
                        </tr>
                      </thead>
                      <tbody>

                        <?php
                            try{
                                $sql=$conexion->prepare("SELECT * FROM Usuarios WHERE role='estudiante'");
                                $sql->execute();

                                while($fila = $sql->fetch()){?>
                                    <tr>
                                        <td><?php echo $fila['username'];?></td>
                                        <td><?php echo $fila['password'];?></td>
                                        <td><?php echo $fila['role'];?></td>
                                        <td><?php echo $fila['correo_electronico'];?></td>
                                        <td>desactivo</td>
                                        <td>
                                          <a href="detalle_alumno.php?id=<?php echo urlencode($fila['id']); ?>" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> Detalle </a>
                                          <a href="form_editar_alumno.php?id=<?php echo urlencode($fila['id']); ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Editar </a>
                                          <a href="eliminar_alumno.php?id=<?php echo urlencode($fila['id']); ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Eliminar </a>
                                        </td>
                                      </tr>
                                <?php
                                } ?>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                    <?php }
      catch(PDOException $e){
        print "Error: ".$e->getMessage()."<br/>";
        die();
                }
              }
 include("includes/footer.php"); ?>
