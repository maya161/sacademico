<?php 
include("includes/header.php");
require_once("conection/conexion.php");
?>    
<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Primero<small></small></h2>
                    <a href="form_adicionar_paralelo_primero.php" class="btn btn-success "> Adicionar Paralelo</a>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  
                  <div class="x_content">                
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Paralelo</th>
                          <th>Cupo</th>
                          <th>Operaciones</th>
                        </tr>
                      </thead>
                      <tbody>

                            
                        <?php  
                        $colores=array("#00FFFF", "#FF00FF", "#000080", "#008080", "#800080", "#008000");
                        $i=0;
                            try{
                                $sql=$conexion->prepare("SELECT * FROM Curso");
                                $sql->execute();

                                while($fila = $sql->fetch()){?>
                                    <tr>
                                        <div class="btn btn-outline-success"><td bgcolor="<?php 
                                            echo $colores[$i];
                                            $i= $i + 1;
                                        ?>"><a href="listar_paralelo.php"><?php echo $fila['paralelo'];?></a></td></div>                                
                                        <td ><?php echo $fila['cupo'];?></td>                                
                                            
                                        <td>                             
                                          <a href="listar_inscritos_primero_primaria.php?id_curso=<?php echo urlencode($fila['id_curso']); ?>" class="btn btn-light btn-xs"><i class="fa fa-trash-o"></i> Asignar Horario</a>
                                          <a href="listar_inscritos_primero_primaria.php?id_curso=<?php echo urlencode($fila['id_curso']); ?>" class="btn btn-dark btn-xs"><i class="fa fa-trash-o"></i> Listar inscritos</a>
                                          <a href="form_editar_curso_primero_primaria.php?id_curso=<?php echo urlencode($fila['id_curso']); ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Editar </a>
                                          <a href="eliminar_primero_primaria.php?id_curso=<?php echo urlencode($fila['id_curso']); ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Eliminar </a>

                                        </td>
                                      </tr>
                                <?php              
                          }   
                            ?>        
                                    </tbody>
                                  </table>
                                </div>
                                
                              </div>
                            </div>
<?php
}    catch(PDOException $e){
      print "Error: ".$e->getMessage()."<br/>";
      die();
              }
 include("includes/footer.php");?>