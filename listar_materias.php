<?php 
include("includes/header.php");
require_once("conection/conexion.php");
?>
<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">

                  <h2>MATERIAS<small></small></h2><br><br>
                  <h2>NIVEL PRIMARIA<small></small></h2>

                    <a href="form_adicionar_materia1.php?nivel=<?php echo urlencode('primaria');?>" class="btn btn-success "> Adicionar</a>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  
                  <div class="x_content">                
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Materia</th>
                          <th>Operaciones</th>
                        </tr>
                      </thead>
                      <tbody>

                            
                        <?php  

                            try{
                                $sql=$conexion->prepare("SELECT * FROM Materia WHERE nivel='primaria'");
                                $sql->execute();

                                while($fila = $sql->fetch()){?>
                                    <tr>
                                        <td><?php echo $fila['sigla'];?></td>                                
                                        <td>                             
                                          <a href="form_editar_materia.php?id_mat=<?php echo urlencode($fila['id_mat']); ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Editar </a>
                                          <a href="eliminar_materia.php?id_mat=<?php echo urlencode($fila['id_mat']); ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Eliminar </a>
                                        </td>
                                      </tr>
                                <?php              
                          }   
                            ?>        
                                    </tbody>
                                  </table>
                                </div>

<?php
}    catch(PDOException $e){
      print "Error: ".$e->getMessage()."<br/>";
      die();
              }
?>
 <h2>NIVEL SECUNDARIA<small></small></h2>
<a href="form_adicionar_materia1.php?nivel=<?php echo urlencode('secundaria');?>" class="btn btn-success "> Adicionar</a>
<ul class="nav navbar-right panel_toolbox">
  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
  </li>
  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
    <ul class="dropdown-menu" role="menu">
      <li><a href="#">Settings 1</a>
      </li>
      <li><a href="#">Settings 2</a>
      </li>
    </ul>
  </li>
  <li><a class="close-link"><i class="fa fa-close"></i></a>
  </li>
</ul>
<div class="clearfix"></div>
</div>

<div class="x_content">                
<table id="datatable" class="table table-striped table-bordered">
  <thead>
    <tr>
      <th>Materia</th>
      <th>Operaciones</th>
    </tr>
  </thead>
  <tbody>

        
    <?php  

        try{
            $sql=$conexion->prepare("SELECT * FROM Materia WHERE nivel='secundaria'");
            $sql->execute();

            while($fila = $sql->fetch()){?>
                <tr>
                    <td><?php echo $fila['sigla'];?></td>                                
                    <td>                             
                      <a href="form_editar_materia.php?id_mat=<?php echo urlencode($fila['id_mat']); ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Editar </a>
                      <a href="eliminar_materia.php?id_mat=<?php echo urlencode($fila['id_mat']); ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Eliminar </a>
                    </td>
                  </tr>
            <?php
      }
        ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
<?php
}    catch(PDOException $e){
print "Error: ".$e->getMessage()."<br/>";
die();
}
include("includes/footer.php");

?>