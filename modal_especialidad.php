
<!-- modal editar especialidad -->
<div class="modal fade" id="edit_esp_<?php echo $fila1['id_especialidad']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header border-bottom-0">
                <h5 class="modal-title" id="exampleModalLabel">Editar informacion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <form novalidate action="update_especialidad.php" method="POST">
                   <input type="number" id= "id_especialidad" name="id_especialidad" value="<?php echo $fila1['id_especialidad']?>" hidden>
                   <div class="modal-body">
                        <div class="form-group" style="display: block;">
                            <label for="especialidad" style="display: block;">Especialidad</label>
                            <input type="text" id="eespecialidad" name="especialidad" value="<?php echo $fila1['especialidad']?>" style="display: block;" class="form-control">
                        </div>
                   </div>
                    <div class="modal-footer border-top-0 d-flex justify-content-center">
                        <a href="listar_especialidad_profesor.php" class="btn btn-danger" data-dismiss="modal">Cancel</a>
                        <button type="submit" class="btn btn-success" data-dismiss="modal">Submit</button>
                    </div>
                </form>
    </div>
</div>
</div>


