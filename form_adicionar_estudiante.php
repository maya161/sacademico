<?php 
include("includes/header.php");
require_once("conection/conexion.php");
?>

    <div class="x_content">
        <form class="form-horizontal form-label-left" action="alta_alumno.php" method="POST" novalidate >
        <?php
            $x=$_GET['id'];
        ?>
        <input type="text" name="id_us" value="<?php echo $x;?>" hidden>

        <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Nombre<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="nombre" required="required" class="form-control col-md-7 col-xs-12">
                </div>
            </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Apellido<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="apellido" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Fecha de nacimiento<span class="required">*</span>
                        </label>    
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="date" name="fecha_nacimiento" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Direccion<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="direccion" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Telefono<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="number" name="telefono" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >CI<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="number" name="ci" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                        <button type="submit" class="btn btn-lg btn-primary">Adicionar</button>
                      </div>
                      </div>
                    </form>
                  </div>
                </div>

<?php
include("includes/footer.php");
?>