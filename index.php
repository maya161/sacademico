<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="css/build/css/bootstrap.min.css"> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
<!-- Minified JS library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Compiled and minified Bootstrap JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<style>
    .carousel{
        width: 800px;
        height: 500px;
    }

    .fondo{
        background: #2A3F54;
    }

    .letra{
        color: white;
    }

    .boton{
        background: #1ABB9C;
    }

    .cabecera_card{
        background: #73879C;
    }

    .cuerpo_card{
        background: #ADB2B5;
    }
    /*  */
</style>
</head>
<body>

 <nav class="navbar navbar-expand-md fixed-top fondo">
        <ul class="navbar-nav mr-auto">
            <img src="images/birrete1.png" width="50px" height="50px" class="mr-4"> 
            <li class="nav-item">   
                <a class="navbar-brand letra" href="#">Sistema Academico</a> 
            </li>
        </ul> 
            <form class="form-inline">
                <a class="btn boton navbar-brand mr-5" href="loguin.php">Sign in</a>

            </form>   
    </nav>

<div id="myCarouselCustom" class="carousel slide m-auto" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarouselCustom" data-slide-to="0" class="active"></li>
            <li data-target="#myCarouselCustom" data-slide-to="1"></li>
            <li data-target="#myCarouselCustom" data-slide-to="2"></li>
        </ol>
    
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <img src="images/11.jpg" width="800px">
                <div class="carousel-caption">
                    <h3>First Slide</h3>
                    <p>This is the first image slide</p>
                </div>
            </div>
      
            <div class="item">
                <img src="images/22.jpg" width="800px">
                <div class="carousel-caption">
                    <h3>Second Slide</h3>
                    <p>This is the second image slide</p>
                </div>
            </div>
            
            <div class="item">
                <img src="images/33.jpg" width="750px">
                <div class="carousel-caption">
                    <h3>Third Slide</h3>
                    <p>This is the third image slide</p>
                </div>
            </div>
        </div>
    
        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!-- Custom Controls -->
    <div class="text-center">
    <a href="javascript:void(0);" id="prevBtn" class="text-muted">Prev Slide</a>
    <a href="javascript:void(0);" id="nextBtn" class="text-muted">Next Slide</a>
</div>

    <div class="container mt-5">
        <div class="card-deck mb-3 text-center">
            <div class="card mb-3 bordeSombreado">
                <div class="card-header cabecera_card">
                    <h4 class="my-0">MISION</h4>
                </div>
                <div class="card-body cuerpo_card">
                    <p>Somos un colegio que ofrece a la sociedad una propuesta educativa que tiene como objetivo la formación  integral del alumno (personal, social y transcendente).</p>
                </div> 
            </div>

            <div class="card mb-3 bordeSombreado">
                    <div class="card-header cabecera_card">
                        <h4 class="my-0 ">VISION</h4>
                    </div>
                    <div class="card-body cuerpo_card">
                        <p>Un Colegio que considere al alumnado el centro de toda su acción educativa, ayudándole a ser el protagonista activo de su proceso formativo.</p>
                    </div> 
                </div>
            
                <div class="card mb-3 bordeSombreado">
                        <div class="card-header cabecera_card">
                            <h4 class="my-0">VALORES</h4>
                        </div>
                        <div class="card-body cuerpo_card">
                            <ul class="list">
                                Trabajo en misión compartida, desde la corresponsabilidad en las tareas y la Coherencia en la acción educativa y la misión del centro.
                            </ul>
                        </div> 
                    </div>
        </div>
    </div>

    <div class="container mt-5">
        <footer class="footer">
            <div class="container">
                <span class="text-muted">Pie de pagina de Bootstrap</span>
            </div>
        </footer>   
    </div>

    <script type="text/javascript">
        $('.carousel').carousel({
             interval: 8000,
             pause:true,
             wrap:false
        });
    </script>
    <script type="text/javascript">
    // Call carousel manually
    $('#myCarouselCustom').carousel();
    
    // Go to the previous item
    $("#prevBtn").click(function(){
        $("#myCarouselCustom").carousel("prev");
    });
    // Go to the previous item
    $("#nextBtn").click(function(){
        $("#myCarouselCustom").carousel("next");
    });
    </script>

</body>
</html>