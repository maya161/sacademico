<?php 
include("includes/header.php");
require_once("conection/conexion.php");
require_once("controllers/controller_horario.php");

$id_curso= $_GET['id_curso'];
echo $id_curso;
?>

<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                  <div class="x_content">

                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Hora</th>
                          <th>Lunes</th>
                          <th>Martes</th>
                          <th>Miercoles</th>
                          <th>Jueves</th>
                          <th>Viernes</th>
                        </tr>
                      </thead>
                      <tbody>
                                    <tr>
                                        <td>
                                          08:00-10:00
                                        </td>
                                        <td >
                                          <?php
                                            try{
                                              $sql=$conexion->prepare("SELECT m.sigla, m.color FROM Horario h, Materia m WHERE h.id_curso= {$id_curso} and m.id_mat=h.id_mat and h.dia='lunes' and h.hora_ini='8:00' and h.hora_fin='10:00'");
                                              $sql->execute();
                                              if($fila = $sql->fetch()){
                                                $var= $fila['sigla'];
                                                $varcol= $fila['color'];
                                                ?>
                                                 <div style="background:<?php echo $varcol; ?>; color:white; text-align: center;"> <?php echo $var; ?></div>
                                                <?php
                                              }
                                            }catch(PDOException $e){
                                                print "Error: ".$e->getMessage();
                                            }
                                            ?>
                                        </td>
                                        <td>
                                        <?php
                                            try{
                                              $sql=$conexion->prepare("SELECT m.sigla, m.color FROM Horario h, Materia m WHERE h.id_curso= {$id_curso} and m.id_mat=h.id_mat and h.dia='martes' and h.hora_ini='8:00' and h.hora_fin='10:00'");
                                              $sql->execute();
                                              if($fila = $sql->fetch()){
                                                $var= $fila['sigla'];
                                                $varcol= $fila['color'];
                                                ?>
                                                 <div style="background:<?php echo $varcol; ?>; color:white; text-align: center;"> <?php echo $var; ?></div>
                                                <?php                                              }
                                            }catch(PDOException $e){
                                                print "Error: ".$e->getMessage();
                                            }
                                            ?>
                                          </td>
                                        <td>
                                            <?php
                                              try{
                                                $sql=$conexion->prepare("SELECT m.sigla FROM Horario h, Materia m WHERE h.id_curso= {$id_curso} and m.id_mat=h.id_mat and h.dia='miercoles' and h.hora_ini='8:00' and h.hora_fin='10:00'");
                                                $sql->execute();
                                                if($fila = $sql->fetch()){
                                                  $var= $fila['sigla'];
                                                  $varcol= $fila['color'];
                                                ?>
                                                 <div style="background:<?php echo $varcol; ?>; color:white; text-align: center;"> <?php echo $var; ?></div>
                                                <?php
                                                }
                                              }catch(PDOException $e){
                                                  print "Error: ".$e->getMessage();
                                              }
                                            ?>
                                        </td>
                                        <td>
                                        <?php
                                            try{
                                              $sql=$conexion->prepare("SELECT m.sigla FROM Horario h, Materia m WHERE h.id_curso= {$id_curso} and m.id_mat=h.id_mat and h.dia='jueves' and h.hora_ini='8:00' and h.hora_fin='10:00'");
                                              $sql->execute();
                                              if($fila = $sql->fetch()){
                                                $var= $fila['sigla'];
                                                $varcol= $fila['color'];
                                                ?>
                                                 <div style="background:<?php echo $varcol; ?>; color:white; text-align: center;"> <?php echo $var; ?></div>
                                                <?php
                                              }
                                            }catch(PDOException $e){
                                                print "Error: ".$e->getMessage();
                                            }
                                            ?>
                                        </td>
                                        <td>
                                          <?php
                                            try{
                                              $sql=$conexion->prepare("SELECT m.sigla FROM Horario h, Materia m WHERE h.id_curso= {$id_curso} and m.id_mat=h.id_mat and h.dia='viernes' and h.hora_ini='8:00' and h.hora_fin='10:00'");
                                              $sql->execute();
                                              if($fila = $sql->fetch()){
                                                $var= $fila['sigla'];
                                                $varcol= $fila['color'];
                                                ?>
                                                 <div style="background:<?php echo $varcol; ?>; color:white; text-align: center;"> <?php echo $var; ?></div>
                                                <?php 
                                              }
                                            }catch(PDOException $e){
                                                print "Error: ".$e->getMessage();
                                            }
                                          ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>10:00-12:00</td>
                                        <td>
                                          <?php
                                            try{
                                              $sql=$conexion->prepare("SELECT m.sigla FROM Horario h, Materia m WHERE h.id_curso= {$id_curso} and m.id_mat=h.id_mat and h.dia='lunes' and h.hora_ini='10:00' and h.hora_fin='12:00'");
                                              $sql->execute();
                                              if($fila = $sql->fetch()){
                                                $var= $fila['sigla'];
                                                $varcol= $fila['color'];
                                                ?>
                                                 <div style="background:<?php echo $varcol; ?>; color:white; text-align: center;"> <?php echo $var; ?></div>
                                                <?php
                                              }
                                            }catch(PDOException $e){
                                                print "Error: ".$e->getMessage();
                                            }
                                          ?>
                                        </td>
                                        <td>
                                        <?php
                                            try{
                                              $sql=$conexion->prepare("SELECT m.sigla FROM Horario h, Materia m WHERE h.id_curso= {$id_curso} and m.id_mat=h.id_mat and h.dia='martes' and h.hora_ini='10:00' and h.hora_fin='12:00'");
                                              $sql->execute();
                                              if($fila = $sql->fetch()){
                                                $var= $fila['sigla'];
                                                $varcol= $fila['color'];
                                                ?>
                                                 <div style="background:<?php echo $varcol; ?>; color:white; text-align: center;"> <?php echo $var; ?></div>
                                                <?php
                                                }
                                            }catch(PDOException $e){
                                                print "Error: ".$e->getMessage();
                                            }
                                          ?>
                                        </td>
                                        <td>
                                        <?php
                                            try{
                                              $sql=$conexion->prepare("SELECT m.sigla FROM Horario h, Materia m WHERE h.id_curso= {$id_curso} and m.id_mat=h.id_mat and h.dia='miercoles' and h.hora_ini='10:00' and h.hora_fin='12:00'");
                                              $sql->execute();
                                              if($fila = $sql->fetch()){
                                                $var= $fila['sigla'];
                                                $varcol= $fila['color'];
                                                ?>
                                                 <div style="background:<?php echo $varcol; ?>; color:white; text-align: center;"> <?php echo $var; ?></div>
                                                <?php
                                                }
                                            }catch(PDOException $e){
                                                print "Error: ".$e->getMessage();
                                            }
                                          ?>
                                        </td>
                                        <td>
                                        <?php
                                            try{
                                              $sql=$conexion->prepare("SELECT m.sigla FROM Horario h, Materia m WHERE h.id_curso= {$id_curso} and m.id_mat=h.id_mat and h.dia='jueves' and h.hora_ini='10:00' and h.hora_fin='12:00'");
                                              $sql->execute();
                                              if($fila = $sql->fetch()){
                                                $var= $fila['sigla'];
                                                $varcol= $fila['color'];
                                                ?>
                                                 <div style="background:<?php echo $varcol; ?>; color:white; text-align: center;"> <?php echo $var; ?></div>
                                                <?php
                                              }
                                            }catch(PDOException $e){
                                                print "Error: ".$e->getMessage();
                                            }
                                          ?>
                                        </td>
                                        <td>
                                        <?php
                                            try{
                                              $sql=$conexion->prepare("SELECT m.sigla FROM Horario h, Materia m WHERE h.id_curso= {$id_curso} and m.id_mat=h.id_mat and h.dia='viernes' and h.hora_ini='10:00' and h.hora_fin='12:00'");
                                              $sql->execute();
                                              if($fila = $sql->fetch()){
                                                $var= $fila['sigla'];
                                                $varcol= $fila['color'];
                                                ?>
                                                 <div style="background:<?php echo $varcol; ?>; color:white; text-align: center;"> <?php echo $var; ?></div>
                                                <?php
                                              }
                                            }catch(PDOException $e){
                                                print "Error: ".$e->getMessage();
                                            }
                                          ?>
                                          </td>
                                    </tr>
                                    <tr>
                                        <td>12:00-14:00</td>
                                        <td>
                                        <?php
                                            try{
                                              $sql=$conexion->prepare("SELECT m.sigla FROM Horario h, Materia m WHERE h.id_curso= {$id_curso} and m.id_mat=h.id_mat and h.dia='lunes' and h.hora_ini='12:00' and h.hora_fin='14:00'");
                                              $sql->execute();
                                              if($fila = $sql->fetch()){
                                                $var= $fila['sigla'];
                                                $varcol= $fila['color'];
                                                ?>
                                                 <div style="background:<?php echo $varcol; ?>; color:white; text-align: center;"> <?php echo $var; ?></div>
                                                <?php
                                              }
                                            }catch(PDOException $e){
                                                print "Error: ".$e->getMessage();
                                            }
                                          ?>
                                        </td>
                                        <td>
                                        <?php
                                            try{
                                              $sql=$conexion->prepare("SELECT m.sigla FROM Horario h, Materia m WHERE h.id_curso= {$id_curso} and m.id_mat=h.id_mat and h.dia='martes' and h.hora_ini='12:00' and h.hora_fin='14:00'");
                                              $sql->execute();
                                              if($fila = $sql->fetch()){
                                                $var= $fila['sigla'];
                                                $varcol= $fila['color'];
                                                ?>
                                                 <div style="background:<?php echo $varcol; ?>; color:white; text-align: center;"> <?php echo $var; ?></div>
                                                <?php
                                              }
                                            }catch(PDOException $e){
                                                print "Error: ".$e->getMessage();
                                            }
                                          ?>
                                        </td>
                                        <td>
                                        <?php
                                            try{
                                              $sql=$conexion->prepare("SELECT m.sigla FROM Horario h, Materia m WHERE h.id_curso= {$id_curso} and m.id_mat=h.id_mat and h.dia='miercoles' and h.hora_ini='12:00' and h.hora_fin='14:00'");
                                              $sql->execute();
                                              if($fila = $sql->fetch()){
                                                $var= $fila['sigla'];
                                                $varcol= $fila['color'];
                                                ?>
                                                 <div style="background:<?php echo $varcol; ?>; color:white; text-align: center;"> <?php echo $var; ?></div>
                                                <?php
                                              }
                                            }catch(PDOException $e){
                                                print "Error: ".$e->getMessage();
                                            }
                                          ?>
                                        </td>
                                        <td>
                                        <?php
                                            try{
                                              $sql=$conexion->prepare("SELECT m.sigla FROM Horario h, Materia m WHERE h.id_curso= {$id_curso} and m.id_mat=h.id_mat and h.dia='jueves' and h.hora_ini='12:00' and h.hora_fin='14:00'");
                                              $sql->execute();
                                              if($fila = $sql->fetch()){
                                                $var= $fila['sigla'];
                                                $varcol= $fila['color'];
                                                ?>
                                                 <div style="background:<?php echo $varcol; ?>; color:white; text-align: center;"> <?php echo $var; ?></div>
                                                <?php
                                              }
                                            }catch(PDOException $e){
                                                print "Error: ".$e->getMessage();
                                            }
                                          ?>
                                        </td>
                                        <td>
                                        <?php
                                            try{
                                              $sql=$conexion->prepare("SELECT m.sigla FROM Horario h, Materia m WHERE h.id_curso= {$id_curso} and m.id_mat=h.id_mat and h.dia='viernes' and h.hora_ini='12:00' and h.hora_fin='14:00'");
                                              $sql->execute();
                                              if($fila = $sql->fetch()){
                                                $var= $fila['sigla'];
                                                $varcol= $fila['color'];
                                                ?>
                                                 <div style="background:<?php echo $varcol; ?>; color:white; text-align: center;"> <?php echo $var; ?></div>
                                                <?php
                                              }
                                            }catch(PDOException $e){
                                                print "Error: ".$e->getMessage();
                                            }
                                          ?>
                                        </td>
                                    </tr>

                      </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
 include("includes/footer.php");?>