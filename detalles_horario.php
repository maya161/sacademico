<?php
include("includes/header.php");
require_once("conection/conexion.php");
?>
<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">

                  <h2><?php echo strtoupper($_GET['sigla']);?><small></small></h2><br><br>

                  <div class="x_content">
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Dia</th>
                          <th>Hora_ini</th>
                          <th>Hora_fin</th>
                          <th>Operaciones</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php

try{
    $sql=$conexion->prepare("SELECT * FROM Horario WHERE id_curso= {$_GET['id_curso']} and id_mat= {$_GET['id_mat']}");
    $sql->execute();
    while($fila = $sql->fetch()){
    ?>
                    <tr>
                                        <td><?php echo $fila['dia'];?>
                                        </td>
                                        <td>
                                            <?php echo $fila['hora_ini'];?>
                                        </td>
                                        <td>
                                            <?php echo $fila['hora_fin'];?>
                                        </td>
                                        <td>
                                            <a type="button"  class="btn btn-info btn-xs" data-toggle="modal" data-target="#form" ><i class="fa fa-pencil"></i>Editar</a>
                                            <a href="eliminar_horario.php?id_horario=<?php echo urlencode($fila['id_horario']);?>&sigla=<?php echo urlencode($_GET['sigla']);?>&id_mat=<?php echo urlencode($fila['id_mat']);?>&id_curso=<?php echo urlencode($fila['id_curso']);?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Eliminar </a>
                                            <div class="modal fade" id="form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                              <div class="modal-dialog modal-dialog-centered" role="document">
                                                  <div class="modal-content">
                                                      <div class="modal-header border-bottom-0">
                                                          <h5 class="modal-title" id="exampleModalLabel">Editar informacion</h5>
                                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                              <span aria-hidden="true">&times;</span>
                                                          </button>
                                                      </div>
                                                      <form class="form-horizontal form-label-left" novalidate action="update_horario.php" method="POST">
                                                            <input type="text" name= "id_horario" value="<?php echo $fila['id_horario'];?>" hidden>
                                                            <input type="text" name= "id_curso" value="<?php echo $fila['id_curso'];?>" hidden>
                                                            <input type="text" name= "id_mat" value="<?php echo $fila['id_mat'];?>" hidden>
                                                            <input type="text" name= "sigla" value="<?php echo $_GET['sigla'];?>" hidden>

                                                        <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dia">Dia<span class="required">*</span>
                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                <select name="dia">
                                                                    <option value="<?php echo $fila['dia']?>"><?php echo $fila['dia']?></option>
                                                                    <option value="lunes">Lunes</option>
                                                                    <option value="martes">Martes</option>
                                                                    <option value="miercoles">Miercoles</option>
                                                                    <option value="jueves">Jueves</option>
                                                                    <option value="viernes">Viernes</option>
                                                                </select>
                                                            </div>
                                                        </div>


                                                          <div class="item form-group">
                                                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hora_ini">Hora_ini<span class="required">*</span>
                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                <select name="hora_ini">
                                                                    <option value="<?php echo $fila['hora_ini']?>"><?php echo $fila['hora_ini']?></option>
                                                                    <option value="8:00">8:00</option>
                                                                    <option value="10:00">10:00</option>
                                                                    <option value="12:00">12:00</option>
                                                                </select>
                                                            </div>
                                                          </div>

                                                          <div class="item form-group">
                                                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hora_fin">Hora_fin<span class="required">*</span>
                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                <select name="hora_fin">
                                                                <option value="<?php echo $fila['hora_fin']?>"><?php echo $fila['hora_fin']?></option>
                                                                    <option value="10:00">10:00</option>
                                                                    <option value="12:00">12:00</option>
                                                                    <option value="14:00">14:00</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                          <div class="ln_solid"></div>
                                                          <div class="form-group">
                                                            <div class="col-md-6 col-md-offset-3">
                                                                <button type="submit" class="btn btn-primary">Cancel</button><br>
                                                                <button id="send" type="submit" class="btn btn-success">Submit</button>
                                                            </div>
                                                          </div>
                                                      </form>
                                                </div>
                                              </div>
                                          </div>

                                          <br>
                                        </td>
                                    </tr>
                                <?php
                          }
                          ?>
                                    </tbody>
                                  </table>
                                </div>
        </div>
    </div>
<?php
}    catch(PDOException $e){
print "Error: ".$e->getMessage()."<br/>";
die();
}
 include("includes/footer.php");?>