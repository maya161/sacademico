<?php 
  include("includes/header.php");
  require_once("conection/conexion.php");
  include('modal_curso.php');
  ?>
<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>NIVEL PRIMARIA<small></small></h2>
                    <a href="form_adicionar_curso.php?nivel=<?php echo urlencode('primaria');?>" class="btn btn-success "> Adicionar</a>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Curso</th>
                          <th>Paralelo</th>
                          <th>Cupo</th>
                          <th>Operaciones</th>
                        </tr>
                      </thead>
                      <tbody>

                        <?php

                            try{
                                $sql=$conexion->prepare("SELECT * FROM Curso WHERE nivel='primaria'");
                                $sql->execute();

                                while($fila = $sql->fetch()){?>
                                    <tr>
                                        <td><span><?php echo $fila['curso'];?></span></td>
                                        <td><span><?php echo $fila['paralelo'];?></span></td>
                                        <td><span><?php echo $fila['cupo'];?></span></td>
                                        <td>
                                         <a href="#edit_<?php echo $fila['id_curso']; ?>" class="btn btn-info btn-xs" data-toggle="modal"><span class="fa fa-pencil"></span>Edit</a>
                                         <a href="#delete_<?php echo $fila['id_curso']; ?>" class="btn btn-danger btn-xs" data-toggle="modal"><span class="fa fa-trash-o"></span> Delete</a>
                                         <a href="realizar_horario.php?id_curso=<?php echo urlencode($fila['id_curso']); ?>" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i> Asignar Horario</a>
                                        </td>
                                        <?php include("modal_curso.php");?>

                                      </tr>
                                <?php
                          }
                            ?>

                      </tbody>
                    </table>
                  </div>
                  <?php
}    catch(PDOException $e){
      print "Error: ".$e->getMessage()."<br/>";
      die();
              }  ?>

                  <h2>NIVEL SECUNDARIA<small></small></h2>
                    <a href="form_adicionar_curso.php?nivel=<?php echo urlencode('secundaria');?>" class="btn btn-success "> Adicionar</a>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  
                  <div class="x_content">
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Curso</th>
                          <th>Paralelo</th>
                          <th>Cupo</th>
                          <th>Operaciones</th>
                        </tr>
                      </thead>
                      <tbody>

                        <?php

                            try{
                                $sql1=$conexion->prepare("SELECT * FROM Curso WHERE nivel='secundaria'");
                                $sql1->execute();

                                while($fila1 = $sql1->fetch()){?>
                                    <tr>
                                        <td><?php echo $fila1['curso'];?></td>
                                        <td><?php echo $fila1['paralelo'];?></td>
                                        <td><?php echo $fila1['cupo'];?></td>
                                        <td>

<!--                                           <button type="button" class="btn btn-info btn-xs edit" value="<?php echo $fila1['id_curso']; ?>"><i class="fa fa-pencil"></i> Edit</button>
 -->

                                          <a href="eliminar_curso.php?id_curso=<?php echo urlencode($fila1['id_curso']); ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Eliminar </a>
                                          <a href="realizar_horario.php?id_curso=<?php echo urlencode($fila1['id_curso']); ?>" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i> Asignar Horario</a>
                                        </td>
                                      </tr>
                                <?php
                          }
                            ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
<?php
}    catch(PDOException $e){
      print "Error: ".$e->getMessage()."<br/>";
      die();
              }
 include("includes/footer.php");
 ?>
