<!-- Modal editar -->
<div class="modal fade" id="edit_<?php echo $fila['id_curso']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header border-bottom-0">
                <h5 class="modal-title" id="exampleModalLabel">Editar informacion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <form novalidate action="update_curso.php" method="POST">
                    <input type="text" id= "id_curso" name="id_curso" value="<?php echo $fila['id_curso']?>" hidden>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="grado" style="display: block;">Grado</label>
                        <input type="text" id="ecurso" name="curso" value="<?php echo $fila['curso']?>" style="display: block;" class="form-control">
                    </div>
                    <div class="form-group" style="display: block;">
                            <label for="paralelo" style="display: block;">Paralelo</label>
                            <input type="text" id="eparalelo" name="paralelo" value="<?php echo $fila['paralelo']?>" style="display: block;" class="form-control">
                        </div>
                        <div class="form-group" style="display: block;">
                                <label for="cupo" style="display: block;">Cupo</label>
                                <input type="text" id="ecupo" name="cupo" value="<?php echo $fila['cupo']?>" style="display: block;" class="form-control">
                            </div>
                        </div>
                        <div class="modal-footer border-top-0 d-flex justify-content-center">
                            <button type="submit" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-success" data-dismiss="modal">Submit</button>
                        </div>
                </form>
    </div>
</div>
</div>

<!-- Modal eliminar -->
<div class="modal fade" id="delete_<?php echo $fila['id_curso']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <center><h4 class="modal-title" id="myModalLabel">Eliminar registro</h4></center>
            </div>
            <div class="modal-body">
            	<p class="text-center">Estas seguro que quieres eliminar a: </p>
				<h2 class="text-center"><?php echo $fila['curso'].' '.$fila['paralelo']; ?></h2>
			</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
<!--                 <a href="delete.php?id=<?php echo $row['id']; ?>" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Yes</a> -->
                <a href="eliminar_curso.php?id_curso=<?php echo urlencode($fila['id_curso']); ?>" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i> Si </a>

            </div>

        </div>
    </div>
</div>
