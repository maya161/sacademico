<?php 
class alumno{
    public $id_us;
    public $nombre;
    public $apellido;
    public $fecha_nacimiento;
    public $direccion;
    public $telefono;
    public $ci;
    public $id_carnet;
    public $cod_foto;
    public $id_apoderado;

    function __construct($id_us, $nom, $ape, $f_nac, $dir, $tel, $c_i, $id_c, $c_fot, $id_apo)
    {
        $this->id_us= $id_us;
        $this->nombre=$nom;
        $this->apellido= $ape;
        $this->fecha_nacimiento=$f_nac;
        $this->direccion=$dir;
        $this->telefono=$tel;
        $this->ci=$c_i;
        $this->id_carnet=$id_c;
        $this->cod_foto=$c_fot;
        $this->id_apoderado=$id_apo;
    }

    function create(){
        require_once("conection/conexion.php");
        $sw= 0;
        try{
            $sql=$conexion->prepare("INSERT INTO Alumno(id_alumno, id_us, nombre, apellido, fecha_nacimiento, direccion, telefono, ci, id_carnet, cod_foto, id_apoderado, tag) VALUES(NULL, '".$this->id_us."', '".$this->nombre."' , '".$this->apellido."' , '".$this->fecha_nacimiento."' , '".$this->direccion."' , '".$this->telefono."' , '".$this->ci."' , NULL , NULL, NULL, '0');");
            $sql->execute();
            $sw= 1;
        }
        catch(PDOException $e){
            print "Error: ".$e->getMessage()."<br/>";
            $sw= 2;
        }
        finally {
        }
            header("location: listar_alumno.php?var=$sw");
        }
}
?>
