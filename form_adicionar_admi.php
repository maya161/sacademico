<?php 
include("includes/header.php");
require_once("conection/conexion.php");
?>
    <div id="modal_administrador" class="modal">
        <div class="flex" id="flex">
            <div class="contenido-modal">
                <div class="modal-header flex">
                    <h2>Datos administrador</h2>
                    <span class="close" id="close">&times;</span>
                </div>
                <div class="modal-body">
                <form class="form-horizontal form-label-left" action="alta_administrador.php" method="POST" novalidate >
      <?php 
          $x=$_GET['id'];
        ?>    
        <input type="text" name="id_us" value="<?php echo $x;?>" hidden>

        <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">No. Item <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="number" id="item" name="item" required="required" class="form-control col-md-7 col-xs-12">
                </div>
            </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >CI <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="ci" name="ci" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                  
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Turno<span class="required">*</span>
                        </label>    
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="turno" name="turno" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                        <button type="submit" class="btn btn-lg btn-primary">Adicionar</button>
                      </div>
                      </div>
                    </form>
                  </div>
                </div>
                </div>
                <div class="footer">
                    <h3>AlexPV Design &copy;</h3>
                </div>
            </div>
        </div>
    </div>

    <script src="main.js"></script>

<?php
include("includes/footer.php");
?>
