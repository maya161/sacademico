<?php
include("includes/header.php");
require_once("conection/conexion.php");
?>
<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">

                  <h2>MATERIAS<small></small></h2><br><br>

                  <div class="x_content">
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Materia</th>
                          <th>Hora_ini</th>
                          <th>Hora_fin</th>
                          <th>Dia</th>
                          <th>Operaciones</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php

try{
    $sql=$conexion->prepare("SELECT * FROM Materia m, Curso c WHERE m.nivel= c.nivel and c.id_curso= {$_GET['id_curso']}");
    $sql->execute();
    while($fila = $sql->fetch()){
    ?>
                    <tr>
                        <form class="form-horizontal form-label-left" action="alta_horario.php" method="POST" novalidate>
                                        <td><?php echo $fila['sigla'];?>
                                            <input name="id_curso" type="text" value="<?php echo $fila['id_curso'];?>" hidden></td>
                                        </td>
                                        <td>
                                        <input name="id_mat" type="text" value="<?php echo $fila['id_mat'];?>" hidden>

                                            <div class="item form-group">
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <select name="hora_ini">
                                                        <option value="">Elija una opcion</option>
                                                        <option value="8:00">8:00</option>
                                                        <option value="10:00">10:00</option>
                                                        <option value="12:00">12:00</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="item form-group">
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <select name="hora_fin">
                                                    <option value="">Elija una opcion</option>
                                                        <option value="10:00">10:00</option>
                                                        <option value="12:00">12:00</option>
                                                        <option value="14:00">14:00</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="item form-group">
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <select name="dia">
                                                        <option value="">Elija una opcion</option>
                                                        <option value="lunes">Lunes</option>
                                                        <option value="martes">Martes</option>
                                                        <option value="miercoles">Miercoles</option>
                                                        <option value="jueves">Jueves</option>
                                                        <option value="viernes">Viernes</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <button class="btn btn-success" type="submit"> Adicionar</button>
                                            <a href="detalles_horario.php?sigla=<?php echo $fila['sigla']?>&id_mat=<?php echo $fila['id_mat']?>&id_curso=<?php echo $fila['id_curso']?>" class="btn btn-primary"> Detalles</button>
                                        </td>
                                    </form>
                                    </tr>
                                <?php
                          }
                          ?>
                                    </tbody>
                                  </table>
                                </div>
        </div>
    </div>
<?php
}    catch(PDOException $e){
print "Error: ".$e->getMessage()."<br/>";
die();
}
 include("includes/footer.php");?>