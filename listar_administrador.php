<?php 
include("includes/header.php");
require_once("conection/conexion.php");
?>    
<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                
                  <div class="x_title">
                    <h2>Administradores<small></small></h2>
                    <a type="button"  class="btn btn-info" data-toggle="modal" data-target="#form1" class="btn btn-success "></i>Adicionar</a>
                    <div class="modal fade" id="form1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header border-bottom-0">
                              <h5 class="modal-title" id="exampleModalLabel">Datos Personales</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <form class="form-horizontal form-label-left" action="alta_usuario.php" method="POST" novalidate >
                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Username <span class="required">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="name" class="form  -control col-md-7 col-xs-12" data-validate-length-range="6" name="username" placeholder="both name(s) e.g Jon Doe" required="required" type="text">
                              </div>
                            </div>

                            <div class="item form-group">
                              <label for="password" class="control-label col-md-3">Password</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="password" type="password" name="password" data-validate-length="6,8" class="form-control col-md-7 col-xs-12" required="required">
                              </div>
                            </div>

                            <input name="role" type="text" value="administrador" hidden>
                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Email <span class="required">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="email" id="email" name="correo_electronico" required="required" class="form-control col-md-7 col-xs-12">
                              </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                              <div class="col-md-6 col-md-offset-3">
                              <button type="submit" class="btn btn-lg btn-primary">Next</button>
                            </div>
                            </div>
                          </form>
                      </div>
                    </div>
                  </div>
                  <br>

                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Username</th>
                          <th>password</th>
                          <th>Role</th>
                          <th>Email</th>
                          <th>Estado</th>
                          <th>Operaciones</th>
                        </tr>
                      </thead>
                      <tbody>


                        <?php

                            try{
                                $sql=$conexion->prepare("SELECT * FROM Usuarios WHERE role='administrador'");
                                $sql->execute();

                                while($fila = $sql->fetch()){?>
                                    <tr>
                                        <td><?php echo $fila['username'];?></td>
                                        <td><?php echo $fila['password'];?></td>
                                        <td><?php echo $fila['role'];?></td>
                                        <td><?php echo $fila['correo_electronico'];?></td>
                                        <td>desactivo</td>
                                        <td>
                                                <a href="detalle_administrador.php?id=<?php echo urlencode($fila['id']);?>" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> Detalle </a>
                                                <a href="form_editar_administrador.php?id=<?php echo urlencode($fila['id']);?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Editar </a>
                                                <a href="eliminar_administrador.php?id=<?php echo urlencode($fila['id']); ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Eliminar </a>
                                        </td>
                                    </tr>
                                <?php }?>
                      </tbody>
                    </table>
                  </div>

                </div>
              </div>
                                <?php
                            }    catch(PDOException $e){
                                print "Error: ".$e->getMessage()."<br/>";
                                die();
                            }
                            ?>

<?php include("includes/footer.php");?>
