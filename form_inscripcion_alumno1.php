<?php
include("includes/header.php");
require_once("conection/conexion.php");
?>

    <div class="x_content">
        <form class="form-horizontal form-label-left" action="form_inscripcion_alumno2.php" method="POST" novalidate >
        <?php
            $x=$_GET['id'];
        ?>
        <input type="text" name="id_al" value="<?php echo $x;?>" hidden>

            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Fecha: <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="date" name="fecha_matricula" required="required" class="form-control col-md-7 col-xs-12">
                </div>
            </div>

            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Turno <span class="required">*</span>
              </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select name="turno">
                    <option value="">Elija una opcion</option>
                    <option value="ma&ntilde;ana">mañana</option>
                    <option value="tarde">tarde</option>
                    <option value="noche">noche</option>
                  </select>
                </div>
            </div>

            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Nivel <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="nivel">
                  <option value="">Elija una opcion</option>
                  <option value="primaria">primaria</option>
                  <option value="secundaria">secundaria</option>
                </select>
              </div>
            </div>


            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-md-offset-3">
                <button type="submit" class="btn btn-lg btn-success">Next</button>
              </div>
            </div>
          </form>
        </div>
      </div>

<?php
include("includes/footer.php");
?>